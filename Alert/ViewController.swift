//  ViewController.swift
//  Alert
//  Created by Viktoriia Skvarko


import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var buttonAlertOneDo: UIButton!
    
    @IBOutlet weak var buttonAlertTwoDo: UIButton!
    
    @IBOutlet weak var buttonAlertActionSheet: UIButton!
    
    @IBOutlet weak var buttonAlertPoleVvod: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.buttonAlertOneDo.layer.cornerRadius = 6
        self.buttonAlertTwoDo.layer.cornerRadius = 6
        self.buttonAlertActionSheet.layer.cornerRadius = 6
        self.buttonAlertPoleVvod.layer.cornerRadius = 6
        
    }
    
    // Alert с одной кнопкой:
    
    func alertOneButtonVizov () {
        let alertOneDo = UIAlertController(title: "Соoбщение", message: "Вы вызвали Alert с одной кнопкой", preferredStyle: .alert)
        alertOneDo.addAction(UIAlertAction(title: "ОК", style: .cancel))
        present(alertOneDo, animated: true)
    }
    
    @IBAction func alertOneButton(_ sender: UIButton) {
        alertOneButtonVizov()
    }
    
    
    // Alert с двумя кнопками, кнопка Вызвать вызывает первый Alert:
    
    func alertTwoButtonVizov() {
        let alertTwoDo = UIAlertController(title: "Соoбщение", message: "Вызвать Alert?", preferredStyle: .alert)
        alertTwoDo.addAction(UIAlertAction(title: "Отменить", style: .cancel))
        alertTwoDo.addAction(UIAlertAction(title: "Вызвать", style: .default, handler: { action in
            return self.alertOneButtonVizov()
        }
        ))
        present(alertTwoDo, animated: true)
    }
    
    @IBAction func alertTwoButton(_ sender: Any) {
        alertTwoButtonVizov()
    }
    
    
    // Alert Action Sheet меню снизу:
    
    func alertActionSheet() {
        let actionSheetLet = UIAlertController(title: "Сообщение", message: "Вызов Action Sheet", preferredStyle: .actionSheet)
        actionSheetLet.addAction(UIAlertAction(title: "Подробнее", style: .default))
        actionSheetLet.addAction(UIAlertAction(title: "Позвонить", style: .default))
        actionSheetLet.addAction(UIAlertAction(title: "Отменить", style: .cancel))
        actionSheetLet.addAction(UIAlertAction(title: "Удалить", style: .destructive))
        present(actionSheetLet, animated: true)
        
    }
    
    @IBAction func alertActionSheetSend(_ sender: Any) {
        alertActionSheet()
    }
    
    
    // Alert с полем для ввода:
    
    func alertPoleVvoda() {
        
        let vvod = UIAlertController(title: "Введите текст", message: nil, preferredStyle: .alert)
        vvod.addTextField()
        let submitAction = UIAlertAction(title: "Отправить сообщение", style: .default) { [unowned vvod] _ in
            _ = vvod.textFields![0]
        }
        vvod.addAction(submitAction)
        present(vvod, animated: true)
    }
    
    @IBAction func alertPoleDlyaVvoda(_ sender: Any) {
        alertPoleVvoda()
    }
    
}

